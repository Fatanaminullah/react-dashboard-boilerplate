import React from 'react';
import {Helmet} from 'react-helmet';

const TitleComponent = ({title}) => {
  return (
    <Helmet>
      <meta charSet="utf-8" />
      <meta
        name="description"
        content="Dashboard Website created using React JS"
      />
      <title>
        {title ? `Dashboard Website - ${title}` : 'Dashboard Website'}
      </title>
    </Helmet>
  );
};

export default TitleComponent;
