import {DeleteFilled, EditFilled, EyeOutlined} from '@ant-design/icons';
import {Col, message, Popconfirm, Row} from 'antd';
import React, {useEffect} from 'react';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {Button, Container, CustomTable, PageTitle} from '../../components';
import {loadingActions, productsActions} from '../../redux/store/actions';
import {history} from '../../utilities';

const {
  showLoading,
  dismissLoading,
  showLoadingTable,
  dismissLoadingTable,
} = loadingActions;
const {getProductList, deleteProducts, setDetailProducts} = productsActions;

const ProductPage = () => {
  const dispatch = useDispatch();
  const {listProducts} = useSelector((state) => state.products);
  const {t} = useTranslation('productsPage');
  useEffect(() => {
    dispatch(getProductList(null, showLoadingTable, dismissLoadingTable));
  }, []);
  const onClickDelete = (req) => {
    dispatch(deleteProducts(req, showLoading, null)).then((res) => {
      message.success(t('deleteSuccess', {name: req.name}));
      dispatch(getProductList(null, null, dismissLoading));
    });
  };
  const createColumns = () => {
    return [
      {
        title: t('name'),
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.length - b.name.length,
      },
      {
        title: t('description'),
        dataIndex: 'description',
        key: 'description',
        sorter: (a, b) => a.description - b.description,
      },

      {
        title: t('action'),
        dataIndex: 'action',
        key: 'action',
        width: '35%',
        render: (index, item) => {
          return (
            <Row>
              <Col xs={24} md={24} lg={24} xl={8}>
                <Button
                  icon={<EyeOutlined />}
                  text={t('buttonDetail')}
                  type="light"
                  rounded
                  size="small"
                  onClick={() => {
                    dispatch(setDetailProducts(item.id, listProducts)).then(
                      () => {
                        history.push({
                          pathname: '/product/detail',
                          state: {type: 'DETAIL', key: 'PRODUCT'},
                        });
                      },
                    );
                  }}
                />
              </Col>
              <Col xs={24} md={24} lg={24} xl={8}>
                <Button
                  icon={<EditFilled />}
                  text={t('buttonEdit')}
                  type="light"
                  rounded
                  size="small"
                  onClick={() => {
                    dispatch(setDetailProducts(item.id, listProducts)).then(
                      () => {
                        history.push({
                          pathname: '/product/edit',
                          state: {type: 'EDIT', key: 'PRODUCT'},
                        });
                      },
                    );
                  }}
                />
              </Col>
              <Col xs={24} md={24} lg={24} xl={8}>
                <Popconfirm
                  title={t('deleteConfirm', {name: item.name})}
                  okText={t('yes')}
                  onConfirm={() => onClickDelete(item)}
                  cancelText={t('no')}>
                  <Button
                    icon={<DeleteFilled />}
                    text={t('buttonDelete')}
                    type="light"
                    rounded
                    size="small"
                  />
                </Popconfirm>
              </Col>
            </Row>
          );
        },
      },
    ];
  };
  return (
    <div>
      <PageTitle
        showButtonAdd
        title={t('title')}
        buttonAddText={t('buttonAdd')}
        buttonAddPages={{
          page: '/product/add',
          params: {type: 'ADD', key: 'PRODUCT'},
        }}
      />
      <Container>
        <CustomTable
          placeholderSearch={t('placeholderSearch')}
          filterBy={['name', 'description']}
          createColumns={createColumns}
          dataSource={listProducts}
        />
      </Container>
    </div>
  );
};

export default ProductPage;
