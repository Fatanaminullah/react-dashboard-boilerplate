import PageNotFound from './PageNotFound';
import LoginPage from './LoginPage';
import DashboardPage from './DashboardPage';
import ProductPage from './ProductPage';
import ProductFormPage from './ProductsFormPage';

export {PageNotFound, LoginPage, DashboardPage, ProductPage, ProductFormPage};
