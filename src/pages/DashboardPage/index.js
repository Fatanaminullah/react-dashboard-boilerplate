import {Row} from 'antd';
import React from 'react';
import {useTranslation} from 'react-i18next';
import {Container, PageTitle} from '../../components';

const DashboardPage = () => {
  const {t} = useTranslation('dashboardPage');
  return (
    <>
      <PageTitle
        title={t('title')}
        showButtonAdd={false}
        showBackButton={false}
      />
      <Container>
        <Row style={{height: '70vh'}}>
          <h1>{t('body')}</h1>
        </Row>
      </Container>
    </>
  );
};

export default DashboardPage;
