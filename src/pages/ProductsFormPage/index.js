import {Col, Form, Input, message, Row} from 'antd';
import React, {useEffect, useState} from 'react';
import {useTranslation} from 'react-i18next';
import {useDispatch, useSelector} from 'react-redux';
import {useLocation} from 'react-router-dom';
import {Button, Container, PageTitle} from '../../components';
import {loadingActions, productsActions} from '../../redux/store/actions';
import {history} from '../../utilities';

const {dismissLoadingButton, showLoadingButton} = loadingActions;
const {createProducts, setDetailProducts, updateProducts} = productsActions;

const ProductsFormPage = () => {
  const dispatch = useDispatch();
  const {detailProducts} = useSelector((state) => state.products);
  const location = useLocation();
  const [pageType, setPageType] = useState({});
  const {t} = useTranslation('productsFormPage');
  useEffect(() => {
    if (location?.state) {
      switch (location.state?.type) {
        case 'ADD':
          setPageType({
            title: t('addTitle'),
            type: 'ADD',
            form: 'add-products',
          });
          break;
        case 'DETAIL':
          setPageType({
            title: t('detailTitle'),
            type: 'DETAIL',
            form: 'detail-products',
            disabled: true,
          });
          break;
        case 'EDIT':
          setPageType({
            title: t('editTitle'),
            type: 'EDIT',
            form: 'edit-products',
          });
          break;
        default:
          setPageType({
            title: t('addTitle'),
            type: 'ADD',
            form: 'add-products',
          });
          break;
      }
    }
    return () => {
      dispatch(setDetailProducts());
    };
  }, [location.state, t]);
  const onFinish = (values) => {
    if (pageType.type === 'ADD') {
      dispatch(
        createProducts(values, showLoadingButton, dismissLoadingButton),
      ).then(() => {
        message.success(t('success'));
        history.push('/product');
      });
    } else {
      dispatch(
        updateProducts(values, showLoadingButton, dismissLoadingButton),
      ).then(() => {
        message.success(t('editSuccess', {name: values.name}));
        history.push('/product');
      });
    }
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div>
      <PageTitle title={pageType?.title} showBackButton />
      <Container>
        <Row justify="center">
          <Col span={18}>
            <Form
              name={pageType?.form}
              initialValues={detailProducts}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}>
              <Form.Item
                name="name"
                label={t('name')}
                labelCol={{span: 6}}
                labelAlign="left"
                rules={[
                  {
                    required: pageType?.type !== 'DETAIL',
                    message: t('required'),
                  },
                  {
                    whitespace: true,
                    message: t('required'),
                  },
                ]}>
                <Input
                  style={{borderRadius: 5}}
                  disabled={pageType?.disabled}
                />
              </Form.Item>
              <Form.Item
                name="description"
                label={t('description')}
                labelCol={{span: 6}}
                labelAlign="left"
                rules={[
                  {
                    required: pageType?.type !== 'DETAIL',
                    message: t('required'),
                  },
                  {
                    whitespace: true,
                    message: t('required'),
                  },
                ]}>
                <Input.TextArea
                  style={{borderRadius: 5}}
                  disabled={pageType?.disabled}
                />
              </Form.Item>
              {pageType.type !== 'DETAIL' && (
                <Form.Item wrapperCol={{span: 4, offset: 20}}>
                  <Button
                    text={t('submit')}
                    block
                    type="primary"
                    htmlType="submit"
                  />
                </Form.Item>
              )}
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default ProductsFormPage;
