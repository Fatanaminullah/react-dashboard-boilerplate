import {LayoutTemplate} from '../../components';
import {
  DashboardPage,
  LoginPage,
  PageNotFound,
  ProductFormPage,
  ProductPage,
} from '../../pages';

export const routeSources = [
  {component: LoginPage, path: '/', exact: true, key: 'LOGIN'},
  {component: LoginPage, path: '/login', exact: true, key: 'LOGIN'},
  {
    component: PageNotFound,
    path: '/404-not-found',
    exact: true,
    key: 'PAGENOTFOUND',
    title: '404 Page Not Found',
  },
  {
    layout: LayoutTemplate,
    path: '',
    child: [
      {
        component: DashboardPage,
        path: '/dashboard',
        exact: true,
        private: true,
        key: 'DASHBOARD',
      },
      {
        component: ProductPage,
        path: '/product',
        exact: true,
        private: true,
        key: 'PRODUCT',
      },
      {
        component: ProductFormPage,
        path: '/product/add',
        exact: true,
        private: true,
        key: 'PRODUCT',
      },
      {
        component: ProductFormPage,
        path: '/product/detail',
        exact: true,
        private: true,
        key: 'PRODUCT',
      },
      {
        component: ProductFormPage,
        path: '/product/edit',
        exact: true,
        private: true,
        key: 'PRODUCT',
      },
    ],
  },
];
