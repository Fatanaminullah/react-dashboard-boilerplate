import {productsTypes} from '../../action-types';

const {SET_PRODUCTS_DETAIL, SET_PRODUCTS_LIST} = productsTypes;

const setProductList = (payload) => ({
  type: SET_PRODUCTS_LIST,
  payload,
});

const getProductList = (request, showLoading, dismissLoading) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      dispatch(
        setProductList([
          {id: 1, name: 'Product 1', description: 'Product 1 Description'},
          {id: 2, name: 'Product 2', description: 'Product 2 Description'},
        ]),
      );
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const createProducts = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const deleteProducts = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const updateProducts = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve();
      dismissLoading && dispatch(dismissLoading());
    }, 1500);
  });
};

const setDetailProducts = (id, list, role) => (dispatch) => {
  return new Promise((resolve, reject) => {
    if (id && list) {
      let result = list.filter((item) => item.id === id)[0];
      if (result) {
        resolve(result);
        dispatch({
          type: SET_PRODUCTS_DETAIL,
          payload: result,
        });
      } else {
        reject(result);
      }
    } else {
      dispatch({
        type: SET_PRODUCTS_DETAIL,
        payload: {},
      });
    }
  });
};
export {
  getProductList,
  createProducts,
  updateProducts,
  deleteProducts,
  setDetailProducts,
};
