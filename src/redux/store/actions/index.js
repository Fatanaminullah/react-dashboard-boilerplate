import * as authActions from './authentication';
import * as layoutActions from './layout';
import * as loadingActions from './loading';
import * as languageActions from './language';
import * as productsActions from './products';
import * as tableActions from './table';

export {
  authActions,
  layoutActions,
  loadingActions,
  languageActions,
  productsActions,
  tableActions,
};
