import {authActionTypes} from '../../action-types';

const {SET_AUTH_DATA} = authActionTypes;

const setAuthData = (payload) => ({
  type: SET_AUTH_DATA,
  payload,
});

const login = (request, showLoading, dismissLoading) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      dismissLoading && dispatch(dismissLoading());
      dispatch(
        setAuthData({
          username: 'admin.iki@email.com',
          roles: 'admin',
          token: 'xxx',
        }),
      );
      resolve();
    }, 1000);
  });
};

export {setAuthData, login};
