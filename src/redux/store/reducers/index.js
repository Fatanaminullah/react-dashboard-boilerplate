import authReducer from './authentication';
import layoutReducer from './layout';
import loadingReducer from './loading';
import languageReducer from './language';
import productsReducer from './products';
import tableReducer from './table';

export {
  authReducer,
  layoutReducer,
  loadingReducer,
  languageReducer,
  productsReducer,
  tableReducer,
};
