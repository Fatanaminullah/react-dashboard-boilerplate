const SET_PAGE = '@table/set-page';
const SET_PAGE_SIZE = '@table/set-page-size';
const SET_FILTER_VALUE = '@table/set-filter-value';
const SET_FILTERED = '@table/set-filtered';

export {SET_PAGE, SET_PAGE_SIZE, SET_FILTER_VALUE, SET_FILTERED};
