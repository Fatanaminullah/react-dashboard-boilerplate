const shell = require('shelljs');
const chalk = require('chalk');
const {exec} = require('child_process');
const fs = require('fs');

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.stdout.write('\n');
let interval = -1;

/**
 * Adds an animated progress indicator
 * @returns {Promise<any>}
 */
function animateProgress() {
  let P = ['\\', '|', '/', '-'];
  let x = 0;
  return setInterval(() => {
    process.stdout.write('\r' + P[x++]);
    x &= 3;
  }, 250);
}

/**
 * Adds mark check symbol
 */
function addCheckMark(callback) {
  process.stdout.write(chalk.green('Done! ✓'));
  if (callback) callback();
}

/**
 * Create environment file
 * @returns {Promise<any>}
 */
function createEnvironmentFile() {
  return new Promise((resolve, reject) => {
    try {
      process.stdout.write('\nCreating environment file...');
      shell.touch('.env.development');
      shell.touch('.env.production');
      fs.writeFileSync(
        '.env.development',
        'REACT_APP_API_URL={{YOUR_API_DEVELOPMENT_URL}}',
        (err) => {
          if (err) console.log(err);
        },
      );
      fs.writeFileSync(
        '.env.production',
        'REACT_APP_API_URL={{YOUR_API_PRODUCTION_URL}}',
        (err) => {
          if (err) console.log(err);
        },
      );
      resolve();
      process.stdout.write('\nSuccessfully create environment file');
    } catch (err) {
      reject(err);
    }
  });
}
/**
 * Remove the current Git repository
 * @returns {Promise<any>}
 */
function removeFile() {
  return new Promise((resolve, reject) => {
    try {
      process.stdout.write('\nRemoving git repository...');
      shell.rm('-rf', 'folder_structure.png');
      shell.rm('-rf', 'setup.js');
      shell.rm('-rf', 'README.md');
      resolve();
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Remove the current Git repository
 * @returns {Promise<any>}
 */
function removeGitRepository() {
  return new Promise((resolve, reject) => {
    try {
      process.stdout.write('\nRemoving git repository...');
      shell.rm('-rf', '.git/');
      resolve();
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * Initialize a new Git repository
 * @returns {Promise<any>}
 */
function initGitRepository() {
  return new Promise((resolve, reject) => {
    process.stdout.write('\nInitializing new git repository...');

    exec('git init', (err, stdout) => {
      if (err) {
        reject(new Error(err));
      } else {
        process.stdout.write('\nInitializing success');
        resolve(stdout);
      }
    });
  });
}

/**
 * Add all files to the new repository
 * @returns {Promise<any>}
 */
function addToGitRepository() {
  return new Promise((resolve, reject) => {
    exec('git add .', (err, stdout) => {
      if (err) {
        reject(new Error(err));
      } else {
        resolve(stdout);
      }
    });
  });
}

/**
 * Initial Git commit
 * @returns {Promise<any>}
 */
function commitToGitRepository() {
  return new Promise((resolve, reject) => {
    process.stdout.write('\nCommitting your first commit....');
    exec('git commit -m "Initial commit"', (err, stdout) => {
      if (err) {
        reject(new Error(err));
      } else {
        resolve(stdout);
      }
    });
  });
}

/**
 * Report the the given error and exits the setup
 * @param {string} error
 */
function reportError(error) {
  clearInterval(interval);

  if (error) {
    process.stdout.write('\n\n');
    process.stderr.write(` ${error}\n`);
    process.exit(1);
  }
}

/**
 * Install all packages
 * @returns {Promise<any>}
 */
function installPackages() {
  return new Promise((resolve, reject) => {
    process.stdout.write(
      '\nInstalling dependencies... (This might take a while)\n',
    );

    setTimeout(() => {
      interval = animateProgress();
    }, 100);
    process.stdout.write('\x1B[?251');
    exec('yarn install', (err) => {
      if (err) {
        reject(new Error(err));
      }
      clearInterval(interval);
      addCheckMark();
      resolve('Packages installed');
      process.stdout.write('\x1B[?25h');
      process.stdout.write('\nPackages installed');
    });
  });
}

/**
 * End the setup process
 */
function endProcess() {
  clearInterval(interval);
  process.stdout.write('\n\nDone! All set up');
  process.stdout.write(
    "\nDon't forget to change your package name in package.json",
  );
  process.exit(0);
}

/**
 * Run
 */
(async () => {
  await removeGitRepository();
  await createEnvironmentFile();
  await installPackages().catch((err) => reportError(err));
  await removeFile();
  try {
    await initGitRepository();
    await addToGitRepository();
    await commitToGitRepository();
  } catch (err) {
    reportError(err);
  }
  endProcess();
})();
